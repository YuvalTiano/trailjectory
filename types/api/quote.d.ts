export type SupportedCurrency = "USD" | "EUR" | "ILS";

export interface CurrRequest {
  from_currency_code: SupportedCurrency;
  to_currency_code: SupportedCurrency;
  amount: number;
}

export type SuccessResponse = {
  exchange_rate: number;
  currency_code: SupportedCurrency;
  amount: number;
};

export type FailResponse = {
  message: string;
};

export type QuoteApiResponse = SuccessResponse | FailResponse;
