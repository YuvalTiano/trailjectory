import { logger } from "./../../utils/logger";
import { withCache } from "./../../utils/cache";
// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";
import {
  CurrRequest,
  QuoteApiResponse,
  SupportedCurrency,
} from "../../types/api";

const EXCHANGE_API_URL = "https://api.apilayer.com/exchangerates_data/convert";
const SECRET_API_KEY = "9n2scMXjU2wuGNtsQgCtrmv1T5NXQXVw";

interface IServiceResponse {
  success: boolean;
  query: { from: SupportedCurrency; to: SupportedCurrency; amount: number };
  info: { timestamp: number; rate: number };
  date: string;
  result: number;
}

const exchangeService = async ({
  amount,
  from_currency_code,
  to_currency_code,
}: CurrRequest) => {
  const res = await fetch(
    `${EXCHANGE_API_URL}?to=${to_currency_code}&from=${from_currency_code}&amount=${amount}`,
    {
      method: "GET",
      redirect: "follow",
      headers: {
        apikey: SECRET_API_KEY,
      },
    }
  );
  const results: IServiceResponse = await res.json();
  return results;
};

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<QuoteApiResponse>
) {
  const { amount, from_currency_code, to_currency_code } =
    req.query as unknown as CurrRequest & { amount: string };
  try {
    const exchangeServiceRes = await withCache<IServiceResponse>(
      from_currency_code + to_currency_code,
      () =>
        exchangeService({
          amount: parseInt(amount, 10),
          from_currency_code,
          to_currency_code,
        })
    );
    if (!exchangeServiceRes.success) {
      logger("Bad Response");
      res.status(404).json({
        message: "Error!",
      });
    }
    res.status(200).json({
      currency_code: to_currency_code,
      exchange_rate: exchangeServiceRes.info.rate,
      amount: parseInt(amount, 10) * exchangeServiceRes.info.rate,
    });
    return;
  } catch (error) {
    logger("Error");
    res.status(404).json({
      message: "Error!",
    });
  }
}
