import { FormControl } from "@mui/material";
import Head from "next/head";
import { useEffect, useState } from "react";
import Select from "@mui/material/Select";
import Button from "@mui/material/Button";
import InputLabel from "@mui/material/InputLabel";
import TextField from "@mui/material/TextField";
import Alert from "@mui/material/Alert";
import Stack from "@mui/material/Stack";
import AlertTitle from "@mui/material/AlertTitle";
import CircularProgress from "@mui/material/CircularProgress";
import MenuItem from "@mui/material/MenuItem";
import styles from "../styles/Home.module.css";
import { SuccessResponse, SupportedCurrency } from "../types/api";

const apiFetcher = async (searchParams: Record<string, string>) => {
  const res = await fetch("/api/quote?" + new URLSearchParams(searchParams));
  const data: SuccessResponse = await res.json();
  return data;
};

const useExchangeFetcher = () => {
  const [amount, setAmount] = useState(1);
  const [fromCurrencyCode, setFromCurrencyCode] =
    useState<SupportedCurrency>("USD");
  const [toCurrencyCode, setToCurrencyCode] =
    useState<SupportedCurrency>("ILS");
  const [amountResults, setAmountResults] = useState(1);
  const [exchangeRate, setExchangeRate] = useState(1);
  const [isLoading, setIsLoading] = useState(false);
  const [isError, setIsError] = useState(false);

  const fetcher = async () => {
    setIsLoading(true);
    try {
      setIsError(false);
      const res = await apiFetcher({
        amount: amount.toString(),
        from_currency_code: fromCurrencyCode,
        to_currency_code: toCurrencyCode,
      });
      setExchangeRate(res.exchange_rate);
      setAmountResults(res.exchange_rate * amount);
    } catch (error) {
      console.log(error);
      setIsError(true);
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    fetcher();
  }, [amount, toCurrencyCode, fromCurrencyCode]);

  const handleRefreshClick = () => {
    fetcher();
  };

  return {
    amount,
    amountResults,
    setAmount,
    fromCurrencyCode,
    setFromCurrencyCode,
    toCurrencyCode,
    setToCurrencyCode,
    exchangeRate,
    isLoading,
    isError,
    handleRefreshClick,
  };
};

const CurrencySelect: React.FC<{
  label: string;
  value: SupportedCurrency;
  onChange: (val: SupportedCurrency) => void;
}> = ({ label, value, onChange }) => {
  return (
    <FormControl>
      <InputLabel id="demo-simple-select-label">{label}</InputLabel>
      <Select
        value={value}
        label={label}
        onChange={(event) => {
          onChange(event.target.value as SupportedCurrency);
        }}
      >
        <MenuItem value="USD">USD</MenuItem>
        <MenuItem value="EUR">EUR</MenuItem>
        <MenuItem value="ILS">ILS</MenuItem>
      </Select>
    </FormControl>
  );
};

export default function Home() {
  const {
    amount,
    amountResults,
    setAmount,
    fromCurrencyCode,
    setFromCurrencyCode,
    toCurrencyCode,
    setToCurrencyCode,
    exchangeRate,
    isLoading,
    isError,
    handleRefreshClick,
  } = useExchangeFetcher();
  return (
    <div className={styles.container}>
      <Head>
        <title>Trailjectory Exchange</title>
        <meta name="description" content="Trailjectory Currency Exchange" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <h5 className={styles.header}>Trialjectory Currency Exchange</h5>
      <Stack spacing={2} direction={{ xs: 'column', md: 'row' }} justifyContent="center">
        <TextField
          label={fromCurrencyCode}
          type="number"
          InputLabelProps={{
            shrink: true,
          }}
          value={amount}
          onChange={(event) => {
            setAmount(parseInt(event.target.value, 10));
          }}
        />
        <CurrencySelect
          label="from"
          value={fromCurrencyCode}
          onChange={setFromCurrencyCode}
        />
        <Button
          disabled={isLoading}
          variant="contained"
          onClick={handleRefreshClick}
        >
          {isLoading ? <CircularProgress /> : "Refresh"}
        </Button>
        <TextField
          label={toCurrencyCode}
          type="number"
          InputLabelProps={{
            shrink: true,
          }}
          disabled
          value={amountResults}
        />
        <CurrencySelect
          label="to"
          value={toCurrencyCode}
          onChange={setToCurrencyCode}
        />
      </Stack>
      {isError && (
        <Alert severity="error">
          <AlertTitle>Error</AlertTitle>
          Please try again later
        </Alert>
      )}
      <div className={styles.rateSection}>
        1 {fromCurrencyCode} = {exchangeRate.toFixed(2)} {toCurrencyCode}
      </div>
    </div>
  );
}
