import { logger } from "./logger";

const _cache: Record<
  string,
  {
    timestamp: number;
    value: unknown;
  }
> = {};

export const withCache = async <T>(
  key: keyof typeof _cache,
  fetcher: () => Promise<T>
) => {
  const revalidateCache = async () => {
    logger("Refetch", "info");
    const res = await fetcher();
    _cache[key] = {
      value: res,
      timestamp: Date.now(),
    };
  };

  if (!_cache[key]) {
    await revalidateCache();
    return _cache[key].value as T;
  }

  const now = Date.now();
  // Need to revalidate the cache every 10 seconds:
  const needToRevalidate = (now - _cache[key].timestamp) / 1000 > 10;
  if (needToRevalidate) {
    await revalidateCache();
  } else {
    logger("Return results from cache", "info");
  }
  return _cache[key].value as T;
};
