type LogLevel = "error" | "warn" | "info" | "debug";

export const logger = (message: string, level: LogLevel = "warn") => {
  console.log(message);
};
